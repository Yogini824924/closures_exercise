//function cacheFunction(cb) {
    // Should return a funciton that invokes `cb`.
    // A cache (object) should be kept in closure scope.
    // The cache should keep track of all arguments have been used to invoke this function.
    // If the returned function is invoked with arguments that it has already seen
    // then it should return the cached result and not invoke `cb` again.
    // `cb` should only ever be invoked once for a given set of arguments.
  //}

  let cache={};
  function cacheFunction(cb) {
 
    return ((...arguments)=>{
        let a=0;
        for(let i=0; i < arguments.length; i++){
            if(cache[arguments[i]] !== "seen") {
                cache[arguments[i]] ="seen";
                a=1;
            }
        }
        //console.log(cache);
        if(a===1){
            cb(...arguments);
        }
        else {
            console.log(cache);
        }
    });


  }


  module.exports = cacheFunction;