//function counterFactory() {
  // Return an object that has two methods called `increment` and `decrement`.
  // `increment` should increment a counter variable in closure scope and return it.
  // `decrement` should decrement the counter variable and return it.
//}

/*let counterFactory=(function() {
	function increment(val) {
		console.log(val+1);
	}
	function decrement(val) {
		console.log(val-1);
	}
	return {increment  : increment, decrement : decrement };
})();

counterFactory.decrement(5);
counterFactory.increment(6);
*/
function counterFactory() {
	return { increment : function(a) { 
	 return ++a;},
	decrement : function(a) {return --a;}}
}

module.exports= counterFactory;
