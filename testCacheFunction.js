const cacheFunction = require("./cacheFunction.js");
let cb = function(...a) {
    let sum=0;
    for(let i=0;i<a.length;i++){
        sum+=a[i];
    }
    console.log(sum);
}

cacheFunction(cb)(1,2,3);
cacheFunction(cb)(4,5);
cacheFunction(cb)(7);
cacheFunction(cb)(1,4,2);
cacheFunction(cb)(3,1);
cacheFunction(cb)(7,8,9);
