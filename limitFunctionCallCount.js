//function limitFunctionCallCount(cb, n) {
  // Should return a function that invokes `cb`.
  // The returned function should only allow `cb` to be invoked `n` times.
  // Returning null is acceptable if cb can't be returned
//}


function limitFunctionCallCount(cb,n) {
		function invoke_cb() {
      for(let i=1;i<=n;i++){
        if(cb(i) === undefined) {
          console.log(null);
        }
        else {
			  console.log(cb(i));
        }
		}}
        if(typeof cb === "function" && typeof n === "number"){
    return invoke_cb();}
    else {
      console.log(null);
    }
}

module.exports = limitFunctionCallCount;
